/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fill.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mstorcha <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/04 14:56:26 by mstorcha          #+#    #+#             */
/*   Updated: 2017/12/04 15:14:20 by mstorcha         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

int				len_buf(char *argv)
{
	int		file;
	int		num;
	char	buf;

	file = open(argv, O_RDONLY);
	if (file == -1)
		return (0);
	num = 0;
	while (read(file, &buf, 1))
		num++;
	return (num);
}

char			*ret_str(int argc, char **argv)
{
	int		file;
	char	*str;
	char	buf[1];
	int		i;

	if (argc != 2)
		return (NULL);
	file = open(argv[1], O_RDONLY);
	if (file == -1 || len_buf(argv[1]) == 0
		|| !(str = ft_strnew(len_buf(argv[1]))))
		return (NULL);
	i = 0;
	while (read(file, &buf, 1))
	{
		str[i] = buf[0];
		i++;
	}
	if (symb_check(str) == 1 || kostyl_ch(str) == 1)
		return (NULL);
	return (str);
}

char			**splitted(char *str)
{
	int		i;
	char	**s;

	if (!(s = ft_blocksplit(str)))
		return (NULL);
	i = 0;
	while (s[i] != 0)
	{
		if (ft_strlen(s[i]) != 20)
			return (NULL);
		i++;
	}
	return (s);
}
