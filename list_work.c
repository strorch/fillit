/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_list_size.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmazaiev <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/05 09:59:47 by mmazaiev          #+#    #+#             */
/*   Updated: 2017/09/05 10:08:01 by mmazaiev         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

int			ft_countlist(t_list *begin_list)
{
	t_list	*list;
	int		i;

	i = 0;
	list = begin_list;
	while (list)
	{
		i++;
		list = list->next;
	}
	return (i);
}

void		free_list(t_list *list)
{
	t_list *tmp;

	while (list)
	{
		tmp = list;
		list = list->next;
		free(tmp);
	}
}

t_element	**return_arr_el(t_list *list)
{
	int			i;
	int			count;
	t_element	**new;

	i = 0;
	count = ft_countlist(list);
	new = (t_element **)malloc(sizeof(t_element*) * (count + 1));
	while (list)
	{
		new[i] = (t_element *)malloc(sizeof(t_element));
		new[i]->data = list->data;
		new[i]->letter = list->letter;
		new[i]->num = i;
		i++;
		list = list->next;
	}
	return (new);
}
