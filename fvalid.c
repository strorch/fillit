/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fill.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mstorcha <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/04 14:56:26 by mstorcha          #+#    #+#             */
/*   Updated: 2017/12/04 15:14:20 by mstorcha         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

void		*print_err(void)
{
	ft_putendl("error");
	exit(1);
}

int			symb_check(char *str)
{
	int i;

	i = 0;
	while (str[i] != 0)
	{
		if (str[i] != '.' && str[i] != '#' && str[i] != '\n')
			return (1);
		i++;
	}
	return (0);
}

int			kostyl_ch(char *str)
{
	int i;

	i = ft_strlen(str) - 1;
	if (str[i] != '\n')
		return (1);
	return (0);
}

int			check_ds(char **str)
{
	int		i;
	int		j;
	int		dots;
	int		sharps;

	i = 0;
	while (str[i])
	{
		dots = 0;
		sharps = 0;
		j = 0;
		while (str[i][j])
		{
			if (str[i][j] == '.')
				dots++;
			if (str[i][j] == '#')
				sharps++;
			j++;
		}
		if (dots != 12 || sharps != 4)
			return (1);
		i++;
	}
	return (0);
}

void		trim(char **str)
{
	int		i;
	char	*tmp;

	i = 0;
	while (str[i] != 0)
	{
		tmp = ft_strtrimch(str[i], '#');
		str[i] = ft_strdup(tmp);
		i++;
	}
	free(tmp);
}
