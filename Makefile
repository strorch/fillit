# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: mstorcha <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2018/01/26 15:48:11 by mstorcha          #+#    #+#              #
#    Updated: 2018/03/02 20:59:40 by mstorcha         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = fillit
LIBA = libft/libft.a

CC = gcc

CFLAGS = -Wall -Wextra -Werror

LIB_DIR = ./libft/
LIB_FLAGS = -L$(LIB_DIR) -lft

VALIDATION_FILES = \
					ft_blocksplit.c \
					ft_makelist.c \
					ft_valid.c \
					fvalid.c \
					read_val.c \
					readf.c \
					rmnewl.c

ALGORYTHM_FILES = \
					ft_solve.c \
					help_func.c \
					list_work.c \
					map.c

SOURCES = main.c $(ALGORYTHM_FILES) $(VALIDATION_FILES)

OPTION = -I.

OBJ = $(SOURCES:.c=.o)

all: $(NAME)

$(NAME): $(LIBA) $(OBJ)
	$(CC) $(OBJ) $(LIB_FLAGS) -o $(NAME)

$(LIBA): lib

lib:
	@make all -C $(LIB_DIR)

clean:
	@make clean -C $(LIB_DIR)
	@/bin/rm -f $(OBJ)

fclean: clean
	@make fclean -C $(LIB_DIR)
	@/bin/rm -f $(NAME)

re: fclean all