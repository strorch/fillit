/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_makelist.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mstorcha <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/28 13:06:25 by mstorcha          #+#    #+#             */
/*   Updated: 2017/12/28 13:07:03 by mstorcha         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

t_list	*ft_create_elem(void *data)
{
	t_list *el;

	if (!(el = ft_memalloc(sizeof(t_list))))
		return (NULL);
	el->data = data;
	el->next = NULL;
	return (el);
}

void	ft_list_push_back(t_list **begin_list, void *data)
{
	t_list *lst;
	t_list *new;

	lst = *begin_list;
	new = ft_create_elem(data);
	if (!lst)
	{
		*begin_list = new;
		return ;
	}
	while (lst->next)
		lst = lst->next;
	lst->next = new;
}
