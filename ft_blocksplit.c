/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fill.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mstorcha <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/04 14:56:26 by mstorcha          #+#    #+#             */
/*   Updated: 2017/12/04 15:14:20 by mstorcha         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"
#include <stdio.h>

static int		num(const char *str)
{
	int i;
	int count;

	i = 0;
	count = 0;
	while (str[i] != '\0')
	{
		if (str[i] == '\n' && str[i + 1] != '\0')
		{
			i++;
			if (str[i] == '\n')
			{
				i++;
				count++;
			}
		}
		i++;
	}
	return (count + 1);
}

static int		size(const char *str, int i)
{
	int count;

	count = 0;
	while (str[i] != 0)
	{
		i++;
		count++;
		if (str[i] == '\n' && str[i + 1] != '\0')
		{
			i++;
			count++;
			if (str[i] == '\n')
				break ;
		}
	}
	return (count);
}

static char		*new_word(const char *str, int i, int size)
{
	int		k;
	char	*word;

	k = 0;
	if (!(word = (char *)malloc(sizeof(char) * (size + 1))))
		return (NULL);
	while (k < size)
		word[k++] = str[i++];
	word[k] = 0;
	return (word);
}

char			**ft_blocksplit(char *str)
{
	int		i;
	int		j;
	char	**ret;
	int		length;

	i = 0;
	j = 0;
	length = ft_strlen(str);
	if (!str || !(ret = (char **)malloc(sizeof(char*) * (num(str) + 1))))
		return (NULL);
	while (i < length)
	{
		if (!(ret[j] = new_word(str, i, size(str, i))))
			return (ft_del_str(ret));
		i += size(str, i) + 1;
		j++;
	}
	ret[j] = 0;
	if (arrlength(ret) != num(str))
		return (ft_del_str(ret));
	return (ret);
}
