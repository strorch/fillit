/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mstorcha <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/05 17:09:51 by mstorcha          #+#    #+#             */
/*   Updated: 2017/12/28 15:39:23 by mmazaiev         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

int		arrlength(char **str)
{
	int i;

	i = 0;
	while (str[i] != 0)
		i++;
	return (i);
}

int		arrwidth(char **str)
{
	int i;

	i = 0;
	while (str[0][i] != 0)
		i++;
	return (i);
}

char	**arrcopy(char **str)
{
	char	**new;
	int		width;
	int		length;
	int		i;
	int		j;

	length = arrlength(str);
	i = -1;
	if (!(new = (char **)malloc(sizeof(char *) * (length + 1))))
		return (NULL);
	while (++i < length)
	{
		j = -1;
		width = arrwidth(str);
		if (!(new[i] = ft_strdup(str[i])))
			return (NULL);
	}
	new[length] = 0;
	return (new);
}

int		main_validation(char **s)
{
	char **copy;

	copy = arrcopy(s);
	if (check_ds(copy) == 1)
	{
		ft_del_str(copy);
		return (1);
	}
	rmnewl(copy);
	trim(copy);
	if (ft_validation(copy) == 1)
	{
		ft_del_str(copy);
		return (1);
	}
	ft_del_str(copy);
	return (0);
}

void	add_letters(char **s)
{
	int		i;
	int		j;
	char	letter;

	i = 0;
	letter = 'A';
	while (s[i] != 0)
	{
		j = 0;
		while (s[i][j] != 0)
		{
			if (s[i][j] == '#')
				s[i][j] = letter;
			j++;
		}
		i++;
		letter++;
	}
}
