/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_solve.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mstorcha <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/02 20:25:31 by mstorcha          #+#    #+#             */
/*   Updated: 2018/03/02 20:25:35 by mstorcha         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

int		check_place(t_map *map, char **data, int i, int j)
{
	int		tmp_j;
	int		tmp_pos_i;
	int		tmp_pos_j;
	int		num_sharps;
	t_point	*tetr_p;

	i--;
	tmp_j = j - 1;
	num_sharps = 0;
	tetr_p = start_dot(data, return_nondot(data));
	tmp_pos_i = tetr_p->x - 1;
	while (++i < map->size && ++tmp_pos_i < 4)
	{
		j = tmp_j;
		tmp_pos_j = tetr_p->y - 1;
		while (++j < map->size && ++tmp_pos_j < 4)
			if (map->array[i][j] == '.' && data[tmp_pos_i][tmp_pos_j] != '.')
				num_sharps++;
	}
	if (num_sharps != 4)
		return (0);
	return (1);
}

void	put_tetri(t_map *map, char **data, int i, int j)
{
	int		tmp_j;
	int		tmp_pos_i;
	int		tmp_pos_j;
	int		num_sharps;
	t_point	*tetr_p;

	i--;
	tmp_j = j - 1;
	num_sharps = 0;
	tetr_p = start_dot(data, return_nondot(data));
	tmp_pos_i = tetr_p->x - 1;
	while (++i < map->size && ++tmp_pos_i < 4)
	{
		j = tmp_j;
		tmp_pos_j = tetr_p->y - 1;
		while (++j < map->size && ++tmp_pos_j < 4)
			if (map->array[i][j] == '.' && data[tmp_pos_i][tmp_pos_j] != '.')
				map->array[i][j] = data[tmp_pos_i][tmp_pos_j];
	}
}

int		solver(t_map *map, t_element **element, int index)
{
	int i;
	int j;

	if (index == map->count)
	{
		print_map(map);
		exit(0);
	}
	i = -1;
	while (++i < map->size)
	{
		j = -1;
		while (++j < map->size)
		{
			if (check_place(map, element[index]->data, i, j))
			{
				put_tetri(map, element[index]->data, i, j);
				solver(map, element, index + 1);
				clear_from_letter(map, element[index]->letter);
			}
		}
	}
	return (0);
}

void	ft_solve(t_list *list)
{
	t_map		*map;
	int			size;
	t_element	**element;

	size = len_of_map(ft_countlist(list));
	map = ft_mapnew(size);
	map->count = ft_countlist(list);
	element = return_arr_el(list);
	while (solver(map, element, 0) == 0)
	{
		size++;
		free_map(map);
		map = ft_mapnew(size);
		map->count = ft_countlist(list);
	}
}
