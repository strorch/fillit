/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   solve.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmazaiev <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/28 15:21:36 by mmazaiev          #+#    #+#             */
/*   Updated: 2017/12/28 15:42:00 by mmazaiev         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

void	clear_from_letter(t_map *map, char let)
{
	int i;
	int j;

	i = -1;
	while (++i < map->size)
	{
		j = -1;
		while (++j < map->size)
			if (map->array[i][j] == let)
			{
				map->array[i][j] = '.';
			}
	}
}

void	add_let_to_lst(t_list *list)
{
	int i;

	i = 0;
	while (list)
	{
		list->letter = 'A' + i;
		i++;
		list = list->next;
	}
}

int		len_of_map(int n)
{
	int		size;
	int		i;

	size = 4 * n;
	i = 0;
	while (i * i < size)
		i++;
	return (i);
}
