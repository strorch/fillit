/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   map.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mstorcha <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/02 20:22:53 by mstorcha          #+#    #+#             */
/*   Updated: 2018/03/02 20:22:57 by mstorcha         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

void		print_map(t_map *map)
{
	int i;

	i = 0;
	while (i < map->size)
	{
		ft_putstr(map->array[i]);
		ft_putchar('\n');
		i++;
	}
}

void		free_map(t_map *map)
{
	int i;

	i = 0;
	while (i < map->size)
		ft_memdel((void **)&(map->array[i++]));
	ft_memdel((void **)&(map->array));
	ft_memdel((void **)&map);
}

t_map		*ft_mapnew(int size)
{
	t_map	*map;
	int		i;
	int		j;

	map = (t_map *)ft_memalloc(sizeof(t_map));
	map->size = size;
	map->array = (char **)ft_memalloc(sizeof(char *) * size);
	i = -1;
	while (++i < size)
	{
		map->array[i] = ft_strnew(size);
		j = -1;
		while (++j < size)
			map->array[i][j] = '.';
	}
	map->free = (t_point *)malloc(sizeof(t_point));
	map->free->x = 0;
	map->free->y = 0;
	return (map);
}

t_point		*start_dot(char **tetri, t_point *point)
{
	int i;
	int j;
	int check;

	check = 0;
	j = point->y;
	while (--j > -1)
	{
		i = point->x - 1;
		while (++i < 4)
		{
			if (tetri[i][j] != '.')
			{
				check++;
				break ;
			}
		}
	}
	point->y = point->y - check;
	return (point);
}

t_point		*return_nondot(char **tetri)
{
	int		i;
	int		j;
	t_point	*start;

	i = -1;
	start = (t_point*)malloc(sizeof(t_point));
	while (tetri[++i] != 0)
	{
		j = -1;
		while (tetri[i][++j] != 0)
		{
			if (tetri[i][j] != '.')
			{
				start->x = i;
				start->y = j;
				return (start);
			}
		}
	}
	return (NULL);
}
