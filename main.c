/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mstorcha <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/02 20:32:31 by mstorcha          #+#    #+#             */
/*   Updated: 2018/03/02 20:32:34 by mstorcha         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

static void	usage(char *name)
{
	ft_putstr("Usage:  ");
	ft_putstr(name);
	ft_putendl(" FILE");
	ft_putendl("Reads tetriminos' from FILE and place them to grid.");
}

static void	wrong_input(char *name)
{
	ft_putstr(name);
	ft_putendl(": missing file operand");
	ft_putstr("Try '");
	ft_putstr(name);
	ft_putendl(" --help' for more information.");
}

int			main(int argc, char **argv)
{
	t_list	*list;

	if (argc == 2)
	{
		if (ft_strequ(argv[1], "--help"))
		{
			usage(argv[0]);
			return (0);
		}
		if (!(list = rusult_list(ret_str(argc, argv))))
		{
			print_err();
			return (1);
		}
		ft_solve(list);
	}
	else
	{
		wrong_input(argv[0]);
	}
	return (0);
}
