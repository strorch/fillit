/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fill.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mstorcha <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/04 14:56:26 by mstorcha          #+#    #+#             */
/*   Updated: 2017/12/04 15:14:20 by mstorcha         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

static char		**initialing(void)
{
	char **cmp;

	cmp = (char**)malloc(sizeof(char*) * 19 + 1);
	cmp[0] = ft_strdup("####");
	cmp[1] = ft_strdup("#...#...#...#");
	cmp[2] = ft_strdup("##..#...#");
	cmp[3] = ft_strdup("##...#...#");
	cmp[4] = ft_strdup("#...#...##");
	cmp[5] = ft_strdup("#...#..##");
	cmp[6] = ft_strdup("###...#");
	cmp[7] = ft_strdup("#.###");
	cmp[8] = ft_strdup("###.#");
	cmp[9] = ft_strdup("#...###");
	cmp[10] = ft_strdup("##..##");
	cmp[11] = ft_strdup("##...##");
	cmp[12] = ft_strdup("##.##");
	cmp[13] = ft_strdup("#...##...#");
	cmp[14] = ft_strdup("#..##..#");
	cmp[15] = ft_strdup("#..###");
	cmp[16] = ft_strdup("###..#");
	cmp[17] = ft_strdup("#...##..#");
	cmp[18] = ft_strdup("#..##...#");
	cmp[19] = NULL;
	return (cmp);
}

int				ft_validation(char **str)
{
	char	**cmp;
	int		i;
	int		j;
	int		check;

	cmp = initialing();
	i = -1;
	while (str[++i])
	{
		check = 0;
		j = -1;
		while (cmp[++j])
		{
			if (ft_strstr((const char*)str[i], (const char*)cmp[j]))
			{
				check = 1;
				break ;
			}
		}
		if (check == 0)
			return (1);
	}
	ft_del_str(cmp);
	return (0);
}

t_list			*rusult_list(char *str)
{
	int		i;
	char	**s;
	t_list	*new;

	if (!str)
		return (NULL);
	if (!(s = splitted(str)))
		return (NULL);
	if (main_validation(s) == 1)
	{
		ft_del_str(s);
		free(str);
		return (NULL);
	}
	add_letters(s);
	i = -1;
	new = NULL;
	while (s[++i])
		ft_list_push_back(&new, ft_strsplit(s[i], '\n'));
	if (ft_countlist(new) > 26)
		return (NULL);
	add_let_to_lst(new);
	free(str);
	ft_del_str(s);
	return (new);
}
