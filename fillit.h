/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fillit.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mstorcha <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/02 20:43:22 by mstorcha          #+#    #+#             */
/*   Updated: 2018/03/04 12:16:40 by mstorcha         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FILLIT_H
# define FILLIT_H

# include <sys/types.h>
# include <sys/stat.h>
# include <fcntl.h>
# include <unistd.h>
# include <stdlib.h>
# include <stdio.h>
# include <string.h>
# include "libft/libft.h"

typedef struct	s_point
{
	int				x;
	int				y;
}				t_point;

typedef struct	s_list
{
	char			**data;
	t_point			*point;
	char			letter;
	struct s_list	*next;
}				t_list;

typedef struct	s_map
{
	int			size;
	char		**array;
	t_point		*free;
	int			count;
}				t_map;

typedef struct	s_element
{
	char	**data;
	char	letter;
	int		num;
}				t_element;

int				main_validation(char **s);
void			add_letters(char **s);
t_element		**return_arr_el(t_list *list);
void			clear_from_letter(t_map *map, char let);
void			add_let_to_lst(t_list *list);
t_point			*return_nondot(char **tetri);
t_point			*start_dot(char **tetri, t_point *point);
int				len_of_map(int n);
void			ft_list_push_back(t_list **begin_list, void *data);
int				arrlength(char **str);
t_list			*ft_create_elem(void *data);
char			*ret_str(int argc, char **argv);
int				len_buf(char *argv);
int				symb_check(char *str);
void			*print_err(void);
int				kostyl_ch(char *str);
char			**ft_blocksplit(char *str);
char			**splitted(char *str);
int				check_ds(char **str);
int				ft_validation(char **str);
void			trim(char **str);
char			*rmchar(char *str, char c);
void			rmnewl(char **str);
int				ft_countlist(t_list *begin_list);
void			print_map(t_map *map);
void			free_map(t_map *map);
void			free_list(t_list *list);
t_map			*ft_mapnew(int size);
void			ft_solve(t_list *list);
t_list			*rusult_list(char *str);

#endif
